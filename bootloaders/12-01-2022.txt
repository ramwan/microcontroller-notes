- Uploaded Arduino Uno bootloader onto blank atmega328p chip.
- Blank chip required 16MHz oscillator with 22pF caps connected to ground.
- Programmer used was the arduino ProMicro - required using the Arduino as ISP (Atmega32U4) setting
- Device signature not found error apparently is due to wrong wiring or oscillator issues
- Reset pin on device to be flashed needs to be only linked to the resetting pin on the programmer
  and not to ground or high.
- Not sure what the extra LED pins on the Arduino-as-ISP sketch are for... LED_HB appears to just
  dim and brighten periodically.

- we can just use the given Arduino tools to do our own compilation and uploading of bootloaders,
  probably the easiest thing to do right now.
- bootloader program is under C:\Program Files (x86)\Arduino\hardware\arduino\avr\bootloaders\optiboot
  with the Makefile giving pretty concise information on where required bins are and options